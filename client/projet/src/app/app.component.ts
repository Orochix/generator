import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'projet random';
  users = [];
  groups = [];


  constructor(private http: HttpClient) {
    this.getUsers();
  }

  getUsers() {
    this.http.get<any[]>("http://localhost:8080/userlist").subscribe(result => {
      this.users = result;
    });
  }

  getGroups(){
    this.http.get<any[]>("http://localhost:8080/group/2").subscribe(result => {
      this.groups = result;
    });
  }
}
